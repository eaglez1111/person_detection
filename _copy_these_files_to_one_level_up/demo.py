
import sys
sys.path.append('./person_detection/samples')
sys.path.append('./person_detection')
import getMasks

import numpy as np
import cv2
from datetime import datetime

image = cv2.imread("R0010018.JPG")

start = datetime.now()
for i in range(5):
    print("######################## - ",i)
    r,image = getMasks.getMasks(image,1)
    print("TimeDiff:",datetime.now()-start)



cv2.namedWindow('Viewer',cv2.WINDOW_NORMAL)
cv2.resizeWindow('Viewer', 2600,1300)
cv2.imshow("Viewer", image)
cv2.waitKey(0)
cv2.destroyAllWindows()

rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
